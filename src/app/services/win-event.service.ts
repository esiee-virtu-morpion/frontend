// win-event.service.ts

import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Piece } from 'src/app/models/piece';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class WinEventService {
  private playerWonSubject = new Subject<Piece>();
  playerWon$ = this.playerWonSubject.asObservable();
  private readonly backendUrl = 'http://acee5587b6ccc42c1ba194eabafda4cb-359938048.eu-west-3.elb.amazonaws.com:3000';

  constructor(private http: HttpClient) {
  }

  public sendScoreToBackend(playerName: string, score: number) {

    const body = { playerName, score };
    console.log("AvantPost ")
    console.log(playerName)
    console.log(score)
    console.log(body)
    return this.http.post(this.backendUrl + '/scores', body).subscribe();
  }
}
