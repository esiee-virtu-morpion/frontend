import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ScoreService {
  private score: number = 0;
  constructor() { }

  get data(): number {
    return this.score;
  }

  set data(val: number) {
    this.score = val;
  }
}
