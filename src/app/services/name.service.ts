import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NameService {
  name: string = "";

  constructor() { }

  get data(): string {
    return this.name;
  }

  set data(val: string) {
    this.name = val;
  }
}
