import { Component } from '@angular/core';
import {Piece} from "./models/piece";
import {WinEventService} from "./services/win-event.service";
import {Subscription} from "rxjs";
import {AppModule} from "./app.module";
import {ScoreService} from "./services/score.service";

@Component({
  selector: 'app-root',
  template: `
    <div>
      <div *ngIf="nameEntered" style="text-align:center;">
        <h1 style="margin-bottom:auto;">Morpion</h1>
        <h2>Score: {{scoreService.data}}</h2>
        <nav style="margin-top: 0.5rem;">
          <a routerLink="turn-based" routerLinkActive="active">Tour à tour</a>
          <div style="width: 1rem; display: inline-block;"></div>
          <a routerLink="ai" routerLinkActive="active">IA</a>
        </nav>
      </div>
      <div *ngIf="nameEntered" style="margin:0 auto; max-width:300px;">
        <router-outlet></router-outlet>
      </div>
      <app-name-form (nameEntered)="onNameEntered($event)"></app-name-form>
    </div>
  `,
  styles: [
    `
      * {
        font-family: 'Calibri', sans-serif;
      }
      nav a {
        display: inline-block;
        color: #000;
        background: #dbeccd;
        padding: 0.5rem 1rem;
        width: 5rem;
        text-decoration: none;
        white-space: nowrap;
      }

      nav a.active {
        background-color: #4caf50;
        color: white;
        font-weight: 500;
      }

      nav a:hover:not(.active) {
        background-color: #8cad70;
        color: white;
      }
    `
  ]
})
export class AppComponent {
  nameEntered: boolean = false;

  constructor(public scoreService: ScoreService) {
  }

  onNameEntered(event: any) {
    this.nameEntered = !this.nameEntered;
  }

  protected readonly AppModule = AppModule;
}
