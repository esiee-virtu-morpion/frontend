import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { BoardComponent } from './board/board.component';
import { CellComponent } from './cell/cell.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { AiBoardComponent } from './ai-board/ai-board.component';
import { NameFormComponent } from './name-form/name-form.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [AppComponent, BoardComponent, CellComponent, AiBoardComponent, NameFormComponent],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {path: 'turn-based', component: BoardComponent},
      {path: 'ai', component: AiBoardComponent},
      {path: '**', redirectTo: ''},
    ]),
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
    }),
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
