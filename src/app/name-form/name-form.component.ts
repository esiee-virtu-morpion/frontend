import { Component, Output, EventEmitter } from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {NameService} from "../services/name.service";

@Component({
  selector: 'app-name-form',
  templateUrl: './name-form.component.html',
  styleUrls: ['./name-form.component.css']
})
export class NameFormComponent {
  @Output() nameEntered = new EventEmitter<string>();
  static firstName: string = '';
  isNameEntered = false;
  nameForm = this.formBuilder.group({
    firstName: '',
  });
  constructor(private formBuilder: FormBuilder, private nameService: NameService) {
  }

  onSubmit() {
    this.nameEntered.emit(NameFormComponent.firstName);
    this.nameService.data = this.nameForm.get('firstName')?.value + "";
    this.isNameEntered = true;
  }
}
