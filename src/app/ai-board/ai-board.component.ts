import {Component, OnInit} from '@angular/core';
import {TicTacToeService} from 'src/app/services/tic-tac-toe.service';
import {Piece} from 'src/app/models/piece';
import {WinEventService} from "../services/win-event.service";
import {AppComponent} from "../app.component";
import {NameFormComponent} from "../name-form/name-form.component";
import {NameService} from "../services/name.service";
import {ScoreService} from "../services/score.service";

@Component({
  selector: 'app-ai-board',
  templateUrl: './ai-board.component.html',
  styleUrls: ['./ai-board.component.css'],
})
export class AiBoardComponent implements OnInit {
  private currentPlayer: Piece = Piece.EMPTY;
  private player: Piece = Piece.X;
  gameOver: boolean = false;
  board: Piece[][] = [];
  statusMessage: string = '';
  aiLevelEasy = true;

  constructor(private readonly svc: TicTacToeService,
              private winEventService: WinEventService,
              private nameService: NameService,
              private scoreService: ScoreService) {}

  ngOnInit(): void {}

  choosePlayer(checked: boolean) {
    this.player = checked ? Piece.X : Piece.O;
  }
  chooseLevel(checked: boolean) {
    this.aiLevelEasy = checked;
  }

  newGame() {
    this.currentPlayer = Piece.X;
    this.gameOver = false;
    this.board = [
      [Piece.EMPTY, Piece.EMPTY, Piece.EMPTY],
      [Piece.EMPTY, Piece.EMPTY, Piece.EMPTY],
      [Piece.EMPTY, Piece.EMPTY, Piece.EMPTY],
    ];

    this.statusMessage = `C'est au tour du joueur ${this.currentPlayer}`;
    if (this.player !== this.currentPlayer) {
      this.aiTurn();
    }
  }

  move(row: number, col: number) {
    if (!this.gameOver && this.board[row][col] === Piece.EMPTY) {
      this.board[row][col] = this.currentPlayer;
      if (this.svc.isDraw(this.board)) {
        this.statusMessage = `Égalité`;
        this.gameOver = true;
      } else if (this.svc.isWin(this.board)) {
        if (this.currentPlayer === Piece.X) {
          this.scoreService.data += 1
          this.winEventService.sendScoreToBackend(this.nameService.data, this.scoreService.data)
        }
        this.hooray();
        this.statusMessage = `Le joueur ${this.currentPlayer} a gagné !`;
        this.gameOver = true;
      } else {
        this.currentPlayer = this.currentPlayer === Piece.O ? Piece.X : Piece.O;
        this.statusMessage = `C'est au tour du joueur ${this.currentPlayer}`;
        if (this.currentPlayer !== this.player) {
          this.aiTurn();
        }
      }
    }
  }

  aiTurn() {
    const emptyCells = this.svc.countOfEmptyCells(this.board);
    if (emptyCells === 9) {
      this.move(Math.floor(Math.random() * 3), Math.floor(Math.random() * 3));
    } else {
      const bestMove = this.svc.minimax(
        this.board,
        this.aiLevelEasy && emptyCells > 5 ? 5 : emptyCells,
        this.player !== Piece.X
      );
      this.move(bestMove.row, bestMove.col);
    }
  }

  private hooray() {
    var audio = new Audio('assets/KidsCheering.mp3');
    audio.play();
  }

  private logBoard() {
    for (let row = 0; row < this.board.length; row++) {
      console.log(this.board[row]);
    }
  }
}
